README for the debian-keyring package
=====================================

Originally written by Lars Wirzenius, liw@iki.fi.  Now maintained by
James Troup <james@nocrew.org>.  Contributions by J.H.M. Dassen (Ray)
<jdassen@wi.LeidenUniv.nl> and Igor Grobman <igor@debian.org>.


Introduction
------------

The Debian project wants developers to digitally sign the announcements of
their packages, to protect against forgeries.  The Debian project maintains
GPG (GNU Privacy Guard) and PGP keyrings with keys of Debian developers.
This is the README for these keyrings.


Background: PGP and GPG
-----------------------

PGP (Pretty Good Privacy) is currently the most widely used public key
cryptography program. Unfortunately, it uses patented algorithms (the
RSA algorithm (asymmetric) and the IDEA algorithm (symmetric)), making
a DFSG-free implementation impossible. GPG (GNU Privacy Guard;
http://www.gnupg.org/) is a DFSG-free cryptography program which is
based on the same concepts as PGP, but which uses unencumbered
cryptographic algorithms.


Getting debian-keyring.{gpg,pgp}
--------------------------------

The current version of debian-keyring.pgp and debian-keyring.gpg are
always available on your nearest debian mirror in
debian/doc/debian-keyring.tar.gz

That file contains the keyrings, signed copy of keyring md5sums and
this README.  The keyring md5sums will be signed by James Troup.
        
Use "gpg --import" or "pgp -ka" to add the keys in a keyring to your
personal keyring.


Generate a key pair
-------------------

GPG and PGP are used for security, and security can be a bit tricky. Please
read the PGP manual (in /usr/doc/pgp on Debian) before generating a key
pair. The actual generation is trivial. You must use at least 1024 bits.

The Debian project will only accept new key pairs if they are GPG
keys.

(It's a key pair, because GPG and PGP use public key cryptography.  One of
the keys is private, one is public. This is all explained in the PGP
manuals.)


Exchange key signatures with other people
-----------------------------------------

If at all possible, meet other Debian developers in person and sign
each other's keys. Geographical and economical challenges often make
this impossible, but if you can do it, please do.  Signing keys means
verifying that the key and the username belong together. The
signatures can allow other people to trust the key. (This is the "web
of trust" stuff the PGP manual explains about.)
        
Also exchange key signatures with many other PGP users. It all helps to
expand and strengthen the PGP web of trust.
        
Do *NOT* sign other people's key unless you have met that person face
to face in real life and seen a good form of ID (e.g. passport,
driver's license) to ensure that the person is who they say they are.


Getting your key into the debian keyring
----------------------------------------

If you are an old debian developer who hasn't uploaded your packages
for a long time, and your key is not in the keyring, send a mail to
keyring-maint@debian.org explaining the situation, and including your
public PGP key.

All new maintainers should apply to new-maintainer@debian.org, and your
key(s) will be added to the keyring as part of the admission process.


Updating your key(s)
--------------------

If your key has been updated, you should send your update to
keyring-maint@debian.org.


Signing your GPG key with your PGP one
--------------------------------------
        
If you already have a PGP key, but only now made a GPG key, you must
sign your GPG key with your PGP one. This can be done as follows:

o Get the gpg-rsa (or gpg-rsaref, if you live in the US) and gpg-idea
  packages and install them.

o Sign your GPG key with your PGP key:
        gpg --load-extension rsa --load-extension idea \
	    --secret-keyring ~/.pgp/secring.pgp \
            --default-key 'Your PGP ID' --sign-key 'Your GPG ID'
